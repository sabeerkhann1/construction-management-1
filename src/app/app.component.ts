import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

interface Property {
  name: string;
  location: string;
  price: number;
  description: string;
  picture: string;
  isEditing?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  properties: Property[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.fetchProperties();
  }

  fetchProperties(): void {
    this.http.get<any>("http://localhost:8080/construction-management/propertiesG")
      .subscribe(
        (response: any) => {
          if (response && response.data) {
            this.properties = response.data;
          } else {
            console.error('Invalid API response format:', response);
          }
        },
        error => {
          console.error('Error fetching properties:', error);
        }
      );
  }

  onFileSelected(event: any): void {
    const file: File = event.target.files[0];
    this.uploadFile(file);
  }

  uploadFile(file: File): void {
    // Implement file upload logic if needed
  }

  addProperty(): void {
    const newProperty: Property = { ...this.newProperty };
    this.http.post<any>("http://localhost:8080/construction-management/propertiesP", newProperty)
      .subscribe(
        (response: any) => {
          // Handle the response properly based on your server's actual response format
          if (response.success) {
            // Assuming the server responds with a success flag and the added property
            this.properties.push(response.property);
            this.clearNewProperty();
          } else {
            console.error('Error adding property:', response.message);
            // Handle error case, if needed
          }
        },
        error => {
          console.error('Error adding property:', error);
          // Handle error case, if needed
        }
      );
  }

  startEditing(property: Property): void {
    property.isEditing = true;
  }

  updateProperty(property: Property): void {
    property.isEditing = false;
    // Implement update logic if needed
  }

  clearNewProperty(): void {
    this.newProperty = {
      name: '',
      location: '',
      price: 0,
      description: '',
      picture: ''
    };
  }

  newProperty: Property = {
    name: '',
    location: '',
    price: 0,
    description: '',
    picture: ''
  };
}
